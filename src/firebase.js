import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
let firebaseConfig = {
      apiKey: "AIzaSyAlCF4vMEc3F-9lNNX9DcmWQkq-89dG0mc",
      authDomain: "the-project-19401.firebaseapp.com",
      projectId: "the-project-19401",
      storageBucket: "the-project-19401.appspot.com",
      messagingSenderId: "324800928076",
      appId: "1:324800928076:web:f3dceda4443896eb96d7e9"
};
firebase.initializeApp(firebaseConfig);
let db = firebase.firestore()
let storage = firebase.storage();
let auth = firebase.auth();
export {
  firebase, db, storage,auth
}